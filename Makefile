TAG = tails:translate
MODULE_PATH = file-system/etc/puppet/modules

image:
	docker build -t $(TAG) ./

# If you are low on bandwidth, you can make copies of git and debian repos
# available on main host.
local:
	docker build -t $(TAG) \
	  --build-arg git_url=http://172.17.0.1:8000 \
	  --build-arg deb_url=http://172.17.0.1:9999/debian \
	  --build-arg weblate_url=http://172.17.0.1:8000/weblate.git \
	  --build-arg tails_url=http://172.17.0.1:8000/tails.git \
	  ./

run:
	docker run $(TAG)

# get puppet modules needed for tails recipes
submodules:
	for repo in \
	    tails stdlib bash etckeeper haveged loginrecords \
	    molly_guard sshd sudo tor vim common puppet apt concat postfix \
	    sysctl timezone inifile \
	    vcsrepo mysql user jenkins reprepro gitolite; do \
		git submodule add \
			https://git-tails.immerda.ch/puppet-$${repo}.git \
			$(MODULE_PATH)/$${repo}; \
	done
	# puppet-augeas is also needed for tails::base, but is only available
	# in github
	git submodule add \
		https://github.com/camptocamp/puppet-augeas.git \
		$(MODULE_PATH)/augeas

apt/tails-apt.asc:
	wget https://git-tails.immerda.ch/puppet-tails/plain/files/apt/keys.d/tails-apt.asc
