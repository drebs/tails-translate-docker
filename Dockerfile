FROM debian:stretch

# allow customizing URLs
ARG git_url=https://git-tails.immerda.ch
ARG deb_url=http://deb.debian.org/debian
ARG weblate_url=https://github.com/WeblateOrg/weblate.git
ARG tails_url=https://git-tails.immerda.ch/tails.git

# configure the machine's hostname
#RUN echo "translate.lizard" > /etc/hostname
#RUN cp /etc/hosts /etc/hosts.new
#RUN sed -i 's/^127\.0\.0\.1.*/127.0.0.1 translate.lizard localhost/' /etc/hosts.new
#RUN sed -i '/^::1/d' /etc/hosts.new
#RUN cp -f /etc/hosts.new /etc/hosts

# configure an initial repository and install some utils
RUN echo "deb ${deb_url} stretch main" > /etc/apt/sources.list
RUN echo "Acquire::ForceIPv4 \"true\";" > /etc/apt/apt.conf
RUN apt-get update
RUN apt-get -y install vim man curl gnupg git apt-transport-https

# install puppet 4
RUN apt-get -y install puppet

# There are some things we need to have in the filesystem tree so we can
# run puppet for the first time and install and setup a fresh Weblate website
# from scratch.
COPY file-system /

# tweak some files to point to correct remote or local locations
RUN sed -i "s@__TAILS_URL__@${tails_url}@" /usr/local/sbin/weblate-setup-project.sh
RUN sed -i "s@__DEB_URL__@${deb_url}@" /etc/puppet/hiera/common.yaml
RUN sed -i "s@__WEBLATE_URL__@${weblate_url}@" /etc/puppet/hiera/common.yaml

# /etc/sysctl.conf must be present
RUN apt-get -y install procps

# install mysql server so mysql_version is available during first puppet run
RUN DEBIAN_FRONTEND=noninteractive apt-get -y install mysql-server

# apply puppet recipes!
RUN puppet apply \
      --hiera_config /etc/puppet/hiera.yaml \
      /etc/puppet/manifests/default.pp

# Weblate is currently using syslog for logging, so we have to have rsyslog
# installed prior to running Weblate.
# TODO: puppetize logging
RUN apt-get -y install rsyslog

# Put Weblate's settings.py in place
COPY weblate/settings_proposed_2.19.1.py /var/lib/weblate/config/settings.py

# Provide the Apache VirtualHost definition that runs in port 80. Note: SSL
# is handled by tails::weblate::reverse_proxy and is configured elsewhere.
# TODO: puppetize apache configuration (there's already a placeholder in
#       tails::weblate).
RUN mkdir -p /var/lib/weblate/config
COPY apache/apache-vhost.conf /var/lib/weblate/config/apache-vhost.conf

# set python3 as default
RUN update-alternatives --install /usr/bin/python python /usr/bin/python3 1

# enable wsgi
RUN a2enmod wsgi

# Install Weblate
RUN /usr/local/sbin/weblate-setup-env.sh; /usr/local/sbin/weblate-install.sh

# Create project and import languages
RUN /usr/local/sbin/weblate-setup-env.sh; /usr/local/sbin/weblate-setup-project.sh

# Build machine translation db and enable use of tmserver
RUN /usr/local/sbin/weblate-build-tmdb.sh
# TODO: Weblate tries to look for languages supported by tmserver by querying
#       ab unexisting API endpoint (/languages), what causes a 404 error and
#       prevents the tmserver functionality in Weblate from working properly.
#       We have to confirm if this is a bug in Weblate, check if this bug is
#       still present in upstream and report/fix accordingly:
#       https://github.com/WeblateOrg/weblate/blob/master/weblate/trans/machine/tmserver.py#L54
COPY weblate/fix-tmserver-api.patch /tmp/
RUN patch -p0 -d /usr/local/share/weblate < /tmp/fix-tmserver-api.patch

# Setup server and serve Weblate using Apache
CMD /usr/local/sbin/weblate-setup-env.sh; /usr/local/sbin/weblate-run-server.sh
