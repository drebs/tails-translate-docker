Tails Translate Docker image
============================

This directory contains files needed to build a Docker_ image containing
a Debian system with the Tails weblate.pp_ Puppet recipe applied that can be
used for development of the translation platform.

.. _Docker: https://docs.docker.com/
.. _weblate.pp: https://git-tails.immerda.ch/puppet-tails/tree/manifests/weblate.pp

Building
--------

To build the image, you first need to `install Docker
<https://docs.docker.com/install/linux/docker-ce/debian/>`_. You may want to
add your user to the ``docker`` group so you don't need to do things as root.

Then, you can initialize git submodules and trigger the build of the Docker
image:

.. code::

  git submodule update --init
  docker build -t tails:translate ./

If you have limited internet connection, you can setup local caches in your
host and pass build arguments to docker builder:

.. code::

  docker build -t tails:translate \
    --build-arg git_url=http://172.17.0.1:8000 \
    --build-arg deb_url=http://172.17.0.1:9999/debian \
    ./

A ``Makefile`` is provided so you can either run ``make`` (or ``make local`` if
you want to use local caches as above) to have the image built with whatever it
needs.

Running
-------

Once you've succesfully built the image, you can run it with:

.. code::

  docker run tails:translate

The Weblate website should be available in something like
``http://172.17.0.2/`` (use your Docker container's IP instead).

You can login using ``admin`` as username and ``123`` as password.

TODO
----

- Test if memcached is working.

- Puppetize cron job for offloaded indexing.
  https://labs.riseup.net/code/issues/15190

- Enable and puppetize strict mode for MariaDB.
  http://django-mysql.readthedocs.io/en/latest/checks.html#django-mysql-w001-strict-mode

- Puppetize settings.py.

- Puppetize apache vhost.

- Puppetize use of rsyslog (or something else) if Weblate will indeed be setup
  to use it for logging.

- Fix upstream Weblate use of tmserver (i.e. there's no /languages in
  tmserver).
