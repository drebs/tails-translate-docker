# translate.tails.boum.org
node 'default' {

  # If no "path" is set, puppet apply fails because "find" (from
  # puppet-apt/manifests/config.php) is not fully qualified.
  Exec {
    path => '/bin:/sbin:/usr/bin:/usr/sbin',
  }

  include tails::base
  include tails::weblate

  # make sure Weblate's MariaDB table character set is utf8
  file { '/etc/mysql/conf.d/50-server.cnf':
    source  => 'puppet:///modules/site_mysql/50-server.cnf',
    require => File['/etc/mysql/conf.d'],
    before  => Mysql_database['weblate'],
    notify  => Service['mysql'],
    owner   => root,
    group   => root,
    mode    => '0664',
  }
}
