#!/usr/bin/env python

# This script creates a "Tails" project in Weblate if it doesn't exist.
#
# Usage:
#
#    WEBLATE_ROOT=/usr/local/share/weblate
#    CREATE_PROJECT_SCRIPT=/usr/local/lib/weblate/create-project-tails.py
#    ${WEBLATE_ROOT}/manage.py shell < ${CREATE_PROJECT_SCRIPT}

from weblate.trans.models.project import Project
from django.http import Http404
from django.shortcuts import get_object_or_404


name = 'Tails'
slug = 'tails'
web = 'https://tails.boum.org/'


try:
    get_object_or_404(Project, slug=slug)
    print('Project with slug "{}" already exists'.format(slug))
except Http404:
    p = Project(name=name, slug=slug, web=web)
    p.save()
    print('Project "{}" created.'.format(name))
