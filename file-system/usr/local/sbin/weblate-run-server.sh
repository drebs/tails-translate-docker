#!/bin/sh

# This script is meant to serve as an entrypoint to the Docker instance and
# serve Weblate in the same way it's done in the production environment.

set -e

weblate_run_server() {
  tmserver -d /var/lib/weblate/tails-wiki.tmdb -p 8080 &
  /usr/sbin/apache2ctl -D FOREGROUND
}

weblate_run_server
