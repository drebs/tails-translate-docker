#!/bin/sh

# This script should be used in a running Docker instance to make sure the
# environment is setup so Weblate can run.
#
# Usually, Docker containers are meant to run one process only but, as our goal
# with this repository and Docker configuration is to test the Puppet recipes
# for Tails Weblate setup, the idea is to mimic the current installation and
# have everything that is set up by Puppet running in the same container.

set -e

weblate_setup_env() {
  service rsyslog start
  service mysql start
}

weblate_setup_env
