#!/bin/sh

# This script performs a series of actions needed for Weblate installation
# See: https://docs.weblate.org/en/latest/admin/install.html

set -e

MANAGE="sudo -u weblate /usr/local/share/weblate/manage.py"

weblate_install() {
  ${MANAGE} collectstatic --noinput
  ${MANAGE} migrate --noinput
  ${MANAGE} createadmin --password 123
}

weblate_install
