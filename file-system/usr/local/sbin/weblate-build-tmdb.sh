#!/bin/sh

# This script creates a Translation Memory Database from the current Tails
# cloned repository.

DBFILE=/var/lib/weblate/tails-wiki.tmdb
REPO=/var/lib/weblate/repositories/vcs/tails/wikisrcindexpo

create_tmdb() {
  for pofile in $( find ${REPO}/wiki/src -iname "*.po" ); do
    lang=$( echo ${pofile} | sed -e "s/^.*\.\([a-z][a-z]\)\.po\$/\1/" );
    build_tmdb -d ${DBFILE} -s en -t ${lang} ${pofile}
  done
}

create_tmdb
