#!/bin/bash

# This script sets up an initial Weblate project and components based on the
# Tails repository. It is meant to be used during development to set up a fresh
# website, as in production the actual Weblate configurations will be loaded
# from a database dump.

# Some imports fail because of unicode error, so for now we will just do as
# much as possible.
#set -e

MANAGE="sudo -u weblate /usr/local/share/weblate/manage.py"
CREATE_PROJECT_SCRIPT=/usr/local/lib/weblate/create-project-tails.py
TAILS_URL="__TAILS_URL__"

weblate_setup_project() {

  # Step 1: create the Weblate "Tails" project.
  #
  # Create the "Tails" project (the construct used below to ensure a new line
  # after the python code is the only reason this script needs to be
  # interpreted by bash).
  cat ${CREATE_PROJECT_SCRIPT} <(echo -e \\n) | ${MANAGE} shell

  LANGS='^(de|ar|ca|zh|es|fr|it|pl|pt|ru|tr|fa)$'
 
  # Step 2: import initial component (and clone from remote repository).
  #
  # Import wiki/src/index as the first component of the project using the
  # remote Tails Git repository. This step clones the remote directory.
  ${MANAGE} import_project \
    --language-regex "${LANGS}" \
    --name-template 'wiki/src/%s.*.po' \
    --component-regexp "wiki\/src\/(?P<name>index)\.(?P<language>.+)\.po" \
    tails ${TAILS_URL} master \
    "wiki/src/**.*.po"

  # Step 3: clone all other components using the same repository as above.
  #
  # Generate a list of directories where .po files live and import each one
  # of them into the Tails project. These steps use the same repository cloned
  # in the step above.
  REPO=/var/lib/weblate/repositories/vcs/tails/wikisrcindexpo/
  for d in $(git -C ${REPO} ls-tree -r --name-only master \
      | grep "\.po$" | grep ^wiki\/src \
      | sed -e 's/\/[^\/]\+\.po//' | sort | uniq); do
    regexp=$(echo ${d} | sed -e "s/\//\\\\\//")
    ${MANAGE} import_project \
      --language-regex "${LANGS}" \
      --name-template "${d}/%s.*.po" \
      --component-regexp "${regexp}\/(?P<name>.*)\.(?P<language>.+)\.po" \
      tails weblate://tails/wikisrcindexpo master \
      "${d}/**.*.po"
  done
}

weblate_setup_project
